<link rel="stylesheet" type="text/css" href="default.css">

<form method="post" enctype="multipart/form-data">
<table>
<?php

$inifile = 'const.ini';

$C = parse_ini_file($inifile,TRUE);
echo '<pre>';
#var_dump($C);
echo '</pre>';

# set new values
array_walk_recursive($C, function (&$v, $k) {
  if (isset($_POST[$k]) and $_POST[$k] != '') {
    $v = $_POST[$k];
  }
}, $C);


# content display
array_walk_recursive($C, function (&$v, $k) {
  echo "<tr>";
  echo "<td>$k</td>";
  echo "<td><input type='text' name='$k' id='$k' value='$v'></input></td>";
  echo "</tr>";
}, $C);


write_php_ini($C, $inifile);


function write_php_ini($array, $file) 
{
  $res = array();
  foreach($array as $key => $val)
    {
      if(is_array($val))
        {
	  $res[] = "\n[$key]";
	  foreach($val as $skey => $sval) $res[] = "$skey = ".(is_numeric($sval) ? $sval : '"'.$sval.'"');
        }
      else $res[] = "$key = ".(is_numeric($val) ? $val : '"'.$val.'"');
    }
  safefilerewrite($file, implode("\r\n", $res));
}

function safefilerewrite($fileName, $dataToSave)
{    if ($fp = fopen($fileName, 'w'))
    {
      $startTime = microtime(TRUE);
        do
	  {            $canWrite = flock($fp, LOCK_EX);
	    // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
	    if(!$canWrite) usleep(round(rand(0, 100)*1000));
	  } while ((!$canWrite)and((microtime(TRUE)-$startTime) < 5));

        //file was locked so now we can store information
        if ($canWrite)
	  {            fwrite($fp, $dataToSave);
            flock($fp, LOCK_UN);
	  }
        fclose($fp);
    }

}
?>

</table>
<input type=submit>
</form>


<html>
<head>
<title>License number cheker</title>
<link rel="stylesheet" type="text/css" href="default.css">
</head>
<body>

<?php

# CONSTANTS 

$C = parse_ini_file('const.ini',TRUE);
$REGION = $C['Region'];
$ERA_CONST = $C['Era_const'];

define('FN_ER', 'bd_era');
define('FN_BD', 'birth_date');
define('FN_LN', 'license_number');


# Posted Parameters

$BD_ERA         = get_param(FN_ER);
$BIRTH_DATE     = get_param(FN_BD);
$LICENSE_NUMBER = get_param(FN_LN);

echo '<pre>'.$C['SBirth_date'].' '.$C[$BD_ERA].$BIRTH_DATE." ";
echo $C['SLicense_number'].$LICENSE_NUMBER.'</pre>';


# Main body

if ($LICENSE_NUMBER) {
  if (strlen($LICENSE_NUMBER) <> 12 ) {
    echo 'Error in Lenth. Please retype.';
  } else {
    echo $C["SRegion"]; # "Licensed in ";
    print_region     ( substr($LICENSE_NUMBER,  0, 2) );
    echo ', ';
    print_lic_year   ( substr($LICENSE_NUMBER,  2, 2) );
    echo '<br/>';
    print_lic_number ( substr($LICENSE_NUMBER,  4, 6) );
    echo ' '. $C['SCheck'].' ';
    print_check_sum  ( $LICENSE_NUMBER );
    echo ', ';
    print_issue_num  ( substr($LICENSE_NUMBER, 11, 1) );
    echo '<hr/>';
  }
}


# Common functions

function get_param($p) {
  $v = $_POST[$p];
  return isset($v) ? $v : '';
}

function styled($class, $str) {
  return '<span class="'.$class.'">'.$str.'</span>';
}

function warning($str) { return styled(__FUNCTION__, $str); }
function caution($str) { return styled(__FUNCTION__, $str); }


# Specific functions

function print_region($code) {
  global $REGION;
  $reg = $REGION[$code];
  echo isset($reg) ? "$reg ($code)": warning($C['SIllegal']);
}

function birth_year() { 
  global $ERA_CONST, $BD_ERA, $BIRTH_DATE;
  $bd_y = substr($BIRTH_DATE, 0, 2);
  return isset($BD_ERA)? $bd_y + $ERA_CONST[$BD_ERA]: -1;
}

function print_lic_year($num) {
  global $C;
  $year = ($num > date('y'))? 1900+$num: 2000+$num;
  $age = $year - birth_year();
  $age_1 = $age - 1;

  $str = "$year ".$C['SAtAge']." $age (or $age_1)";
  if (date('Y') < $year) {
     echo warning($C['SIllegal']);
     echo '<br/>';
  }
  echo ($age<16) ? warning($str): ($age<18)? caution($str) : $str;
}

function print_lic_number($num) {
  global $C;
  echo $C['SSerial']. " ".$num;
#  echo '<br/>';
}

function print_check_sum($num) {
  $sum = substr($num, 10, 1);
  $to_check = str_split('', substr($num, 0, 10));
  $chk = 0;
  $x = array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2);
  for ($i=0; $i<length; $i++) {
    $chk += $to_check[$i] * $x[$i];
  }
  if ($sum == (11 - $chk % 11) % 10) {
    echo 'Ok';
  } else {
    echo warning($C['SIllegal']."$chk<>$sum");
  }
}

function print_issue_num($num) {
  global $C;
  echo ($num == 0) ? $C['SOriginal'] : caution($C["SReIssued"] . $num . $C["STimes"]);
}

?>

<form method="post" enctype="multipart/form-data">
<table><tr>
<td><label for=<?=FN_BD?>><?=$C["SBirth_date"]?>:</label><br/> <?=$C["SExample"]?>41.2.3 => 410203</td>
<td>
<input type='radio' name=<?=FN_ER?> value='Showa'><?=$C["SShowa"]?></input>
<input type='radio' name=<?=FN_ER?> value='Heisei'><?=$C["SHeisei"]?></input>
<br/>
<input type='text' name=<?=FN_BD?> id=<?=FN_BD?>></input>
</td>
</tr><tr>
<td><label for=<?=FN_LN?>><?=$C["SLicense_number"]?>:</label></td>
<td><input type='text' name=<?=FN_LN?> id=<?=FN_LN?>></input></td>
</tr></table>
<input type='submit'>
</form>
</body>
</html>
